# lego-database

## Description
This repo contains an example implementation of the Databricks Medallion Architecture (bronze, silver, gold) utilzing AWS and Databricks Community Edition. This project is based on Kaggle's Lego Database dataset: https://www.kaggle.com/datasets/rtatman/lego-database

## Following Along
You have several options when following along with this repo:
1. View the completed code under [notebooks/solutions](https://gitlab.com/data-engineering-group/lego-data/-/tree/main/notebooks/solutions)
2. Follow along and fill-in the commands under [notebooks/problems](https://gitlab.com/data-engineering-group/lego-data/-/tree/main/notebooks/problems)
3. Offer suggestions and further implementations by submitting [issues](https://gitlab.com/data-engineering-group/lego-data/-/issues) or a [merge request](https://gitlab.com/data-engineering-group/lego-data/-/merge_requests) 

**Suggestion**: Import notebooks to Databricks to view the full rendering of Databricks visualizations

## [Dashboard](https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/1076521054106460/2814194513526971/6919574917410190/latest.html)
This dashboard contains a collection of visualizations generated from the lego-database gold tables. Unfortunately Jupyter Notebook does not properly render Databricks visualizations, so please view exercise 4's results [here](https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/1076521054106460/2814194513526971/6919574917410190/latest.html)

## Contributing
This repo contains sample implementations of the Databricks Medallion Architecture. There are many ways to implement the architecture, and many other implementations, queries, and functions that can be utilized. If you would like to offer suggestions or implement new ideas, please submit a [merge request](https://gitlab.com/data-engineering-group/lego-data/-/merge_requests) or [issue](https://gitlab.com/data-engineering-group/lego-data/-/issues)
